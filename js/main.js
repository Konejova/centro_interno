$('.carousel').carousel({
    interval: 2500
});
//semestres.html
$(document).ready(function () {
    $('[rel="tooltip"]').tooltip({ trigger: "hover" });
});
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
$(function(){
    $('#registro').on('show.bs.modal',function(e){
        console.log('El modal registro se esta mostrando');
        $('#button_registrarse').removeClass('btn-success');
        $('#button_registrarse').addClass('btn-primary');
        $('#button_registrarse').prop('disabled',true);
    });
    $('#registro').on('shown.bs.modal',function(e){
        console.log('El modal registro se mostró');
    });
    $('#registro').on('hide.bs.modal',function(e){
        console.log('El modal registro se oculta');
        $('#button_registrarse').removeClass('btn-primary');
        $('#button_registrarse').addClass('btn-success');
        $('#button_registrarse').prop('disabled',false);
    });
    $('#registro').on('hidden.bs.modal',function(e){
        console.log('El modal registro se ocultó');
    });
})